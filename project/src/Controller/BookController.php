<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\BookType;
use App\Service\BookService;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Repository\BookRepository;
use Symfony\Component\String\Slugger\SluggerInterface;

class BookController extends AbstractController
{


    /**
     * @Route("/", name="main_page")
     * @Template
     * @param BookRepository $bookRepository
     * @return array
     */
    public function mainPage(BookRepository $bookRepository)
    {
        return [];
    }


    /**
     * @Route("/books", name="books")
     * @Template
     * @param BookRepository $bookRepository
     * @return array
     */
    public function index(BookRepository $bookRepository)
    {
        $books = $bookRepository->findAll();

        return [
            'books' => $books
        ];
    }


    /**
     * @Route("/books/list", name="books_list")
     * @param BookService $bookService
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getCountBooksByAuthor(BookService $bookService) {
        return new JsonResponse(
            $bookService->getCountBooksByAuthor()
        );
    }

    /**
     * @Route("/authors/without-books", name="authors_without_books")
     * @param BookService $bookService
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getAuthorsWithoutBooks(BookService $bookService) {
        return new JsonResponse(
            $bookService->getAuthorsWithoutBooks()
        );
    }

    /**
     * @Route("/books/by-day", name="books_by_day")
     * @param BookService $bookService
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getBooksByDay(BookService $bookService) {
        return new JsonResponse(
            $bookService->getBooksByDay()
        );
    }
    /**
     * @Route("/books/load", name="books_load")
     * @param Request $request
     * @param BookService $bookService
     * @param FileUploader $fileUploader
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function load(Request $request, BookService $bookService, FileUploader $fileUploader)
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $bookFile */
            $bookFile = $form->get('filename')->getData();
            if ($bookFile) {
                $fileName = $fileUploader->upload($bookFile);
                $book->setFilename($fileName);

                if ($bookService->load($book)) {
                    return $this->redirect($this->generateUrl('books'));
                } else {
                    $form->addError(new FormError('Не удалось извлечь данные о книге из файла'));
                }
            }
        }

        return $this->render('book/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
