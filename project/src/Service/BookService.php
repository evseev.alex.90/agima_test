<?php

namespace App\Service;

use App\Entity\Author;
use App\Entity\Book;
use App\Service\BookParser\IBookParser;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BookService
{
    private $em;
    private $container;
    private $logger;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->container = $container;
        $this->logger = $logger;
    }

    public function load(Book $book)
    {
        try {
            /** @var IBookParser $parser */
            $parser = $this->container->get('book_parser');
            $bookDir = $this->container->getParameter('books_directory');

            $bookData = $parser->parse($bookDir . DIRECTORY_SEPARATOR . $book->getFilename());

            if (!empty($bookData)) {
                $book->setTitle(@$bookData['title']);

                $author = $this->em->getRepository(Author::class)->createOrReadByName(@$bookData['author']);

                $book->setAuthor($author);

                $book->setLang(@$bookData['language']);

                $this->em->persist($book);
                $this->em->flush();
            } else {
                throw new  \Exception("Не удалось извлеч данные о книге");
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            dump($e->getMessage());die;
            return false;
        }
        return true;
    }


    public function getCountBooksByAuthor() {
        /** @var QueryBuilder $qbAuthor */
        $qbAuthor = $this->em->getRepository(Author::class)->createQueryBuilder('a');

        $qbAuthor
            ->select(['a.name as author', 'COUNT(b.id) as number_books'])
            ->leftJoin(Book::class, 'b', Join::WITH, 'b.author = a.id')
            ->addGroupBy('a.id')
        ;
        return $qbAuthor->getQuery()->getArrayResult();
    }

    public function  getAuthorsWithoutBooks(){
        /** @var QueryBuilder $qbAuthor */
        $qbAuthor = $this->em->getRepository(Author::class)->createQueryBuilder('a');
        /** @var QueryBuilder $sub */
        $sub = $this->em->getRepository(Book::class)->createQueryBuilder('b');


        $sub->select("b");
        $sub->andWhere('b.author = a.id');

        $qbAuthor
            ->select(['a.name as name'])
            ->andWhere($qbAuthor->expr()->not($qbAuthor->expr()->exists($sub->getDQL())))
        ;
        return $qbAuthor->getQuery()->getArrayResult();
    }

    public function getBooksByDay() {
        /** @var QueryBuilder $qbBook */
        $qbBook = $this->em->getRepository(Book::class)->createQueryBuilder('b');

        $qbBook
            ->select(['DATE(b.createdAt)  as createdDay', 'COUNT(b.id) as numberBooks'])
            ->addGroupBy('createdDay')
        ;
        $books = $qbBook->getQuery()->getArrayResult();
        $result = [];
        /** @var Book $r */
        foreach ($books as $r) {
            $result[$r['createdDay']] = $r['numberBooks'];
        }

        return $result;
    }
}
