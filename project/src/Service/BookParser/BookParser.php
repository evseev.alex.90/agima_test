<?php

namespace App\Service\BookParser;

class BookParser implements IBookParser
{

    public function parse($filename): array
    {
        $result = array();
        switch (strtolower(pathinfo($filename, PATHINFO_EXTENSION))) {
            case 'epub' :
                $epubParser = new EpubParser();
                $result = $epubParser->parse($filename);
                break;
            case 'fb2':
                $fb2Parser = new Fb2Parser();
                $result = $fb2Parser->parse($filename);
                break;
            default:
                throw new \Exception('Not found type file');
        }

        return $result;
    }
}
