<?php

namespace App\Service\BookParser;

interface IBookParser  {
    public function parse($filename): array;
}
