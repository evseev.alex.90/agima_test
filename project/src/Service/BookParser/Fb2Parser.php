<?php

namespace App\Service\BookParser;

class Fb2Parser implements IBookParser
{

    public function parse($filename): array
    {
        $result = array();

        $meta = file_get_contents($filename);

        preg_match('#<book-title>(.*?)</book-title>#uis', $meta, $matches);
        $result['title'] = @$matches[1];

        preg_match('#<author>\s*<first-name>(.*?)</first-name>\s*<last-name>(.*?)</last-name>\s*</author>#uim', $meta, $matches);
        $result['author'] = join(' ', [@$matches[1], @$matches[2]]);

        preg_match('#<lang>(.*?)</lang>#uis', $meta, $matches);
        $result['language'] = @$matches[1];

        return $result;
    }
}
