<?php

namespace App\Service\BookParser;


use ZipArchive;

class EpubParser implements IBookParser
{

    public function parse($filename): array
    {
        $result = array();
//
        $zip = new ZipArchive();

        if ($zip->open($filename)) {
            $meta = $zip->getFromName('OEBPS/content.opf');

            preg_match('#<dc:title>(.*?)</dc:title>#uis', $meta, $matches);
            $result['title'] = $matches[1];

            preg_match('#<dc:creator>(.*?)</dc:creator>#uis', $meta, $matches);
            $result['author'] = $matches[1];

            preg_match('#<dc:language>(.*?)</dc:language>#uis', $meta, $matches);
            $result['language'] = $matches[1];
        }

        return  $result;
    }
}
